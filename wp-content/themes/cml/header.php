<!DOCTYPE html>
<html>

<head>
	<?php wp_head(); ?>
	<title>Cloud My Lab || Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/style.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/style-fonts.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/animate.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/owl.carousel.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/owl.theme.default.css';?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri().'/img/logo/logo.png';?>">
	
</head>

<body>

<!-- Header Section -->	
<?php 
	$slug = basename(get_permalink()); 

	if($slug != 'login' && $slug != 'sign-up' && $slug != 'forgot-password'){


?>

<header class="header">
	<div class="container-fluid">
		<div class="container_space">
			<nav class="navbar navbar-expand-lg">
				<a class="navbar-brand" href="<?php echo home_url();?>">

					<?php 
						$image = get_field('header_logo','options');
					?>
					<img src="<?php echo esc_url($image['url']);?>">
				</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<i class="fas fa-bars"></i>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<?php
						wp_nav_menu( array(
						    'menu'   => 'Main Menu',
						    'container' => false,
						    'menu_class'=> 'navbar-nav ml-auto',
						) );
					?>
				</div>

				<ul class="navbar-nav extra_nav d-none d-lg-block d-xl-block">
				  <li><a href="javascript:void();"><i class="fas fa-search"></i></a></li>
				  <li><a href="javascript:void();"><i class="fas fa-shopping-bag"></i></a></li>
				</ul>		
			</nav>
		</div>
	</div>
</header>	

<?php } ?>
<!-- Header Section End -->