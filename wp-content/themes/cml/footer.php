<?php 
	$slug = basename(get_permalink()); 

	if($slug != 'login' && $slug != 'sign-up' && $slug != 'forgot-password'){

?>

<!-- What People Say Section -->	
<div class="need_help_sec">
	<div class="container-fluid">
		<div class="need_help_wrap text-center">
			<?php echo the_field('support_team_description','options');?>
		</div>
	</div>
</div>
<!-- What People Say Section End -->

<!-- Footer Section -->
<footer class="footer">
	<div class="container-fluid">
		<div class="container_space foot_space">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3><?php echo the_field('contact_title','options');?></h3>
						<?php echo the_field('contact_description','options');?>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3><?php echo the_field('quick_link_title','options');?></h3>
						<?php
							wp_nav_menu( array(
							    'menu'   => 'Quick Link',
							    'container' => false,
							    'items_wrap' => '<ul>%3$s</ul>',
							) );
						?>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3><?php echo the_field('important_link_title','options');?></h3>
						<?php
							wp_nav_menu( array(
							    'menu'   => 'Important Link',
							    'container' => false,
							    'items_wrap' => '<ul>%3$s</ul>',
							) );
						?>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3><?php echo the_field('subscribe_us_title','options');?></h3>
						<div class="subs_box">
							<form>
								<input type="email" placeholder="Email">
								<span><i class="fas fa-paper-plane"></i></span>
							</form>
						</div>
						<h3><?php echo the_field('follow_us_title','options');?></h3>
						<div class="social_box">
							<ul>
								<li><a href="javascript:void();"><i class="fab fa-facebook-f"></i></a></li>															
								<li><a href="javascript:void();"><i class="fab fa-twitter"></i></a></li>															
								<li><a href="javascript:void();"><i class="fab fa-instagram"></i></a></li>															
								<li><a href="javascript:void();"><i class="fab fa-pinterest-p"></i></a></li>															
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Footer Section End -->

<?php } ?>

<?php wp_footer();?>
<script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
<script src="<?php echo get_template_directory_uri().'/js/popper.min.js';?>"></script>
<script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri().'/js/owl.carousel.min.js';?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
$(document).ready(function(){
	$('.people_say_slider').owlCarousel({
		items:3,
		loop:true,
		margin:30,
		nav:false,
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
			},
			600:{
				items:2,
			},
			1000:{
				items:3,
			}
		}
	});
});
</script>

<script>

	jQuery(document).on('click','#create-account',function(){
		var reqlength = $('.input_1').length;
	    var value = $('.input_1').filter(function () 
	    {
	        return this.value != '';
	    });

	    if (value.length>=0 && (value.length !== reqlength)) 
	    {
	        toastr.error('Please fill out all required fields.');
	    } 
		else 
	    {
	        var form = $('#formdata').serialize();
			jQuery(this).attr('disabled',true);
			jQuery(this).html('Processing...');
			jQuery.ajax({
		        type: "POST",
		        url: "/wp-admin/admin-ajax.php",
		        data: {
		            action: 'folder_contents',
		            variable: form 
		        },
		        dataType : "json",
		        success: function (data) 
		        {
		        	jQuery(this).attr('disabled',false);
					jQuery(this).html('Submit');
		        	if(data.success == true)
		        	{
		        		toastr.success(data.msg);
		        		window.location.href = data.url; 
		        	}
		        	else
		        	{
		        		toastr.error(data.msg);
		        	}
		           
		        }
		    });
	    }
	});

</script>

<script>
	new WOW().init();
</script>
	 
</body>
</html>