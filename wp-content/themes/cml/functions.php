<?php
	if( function_exists('acf_add_options_page') ) 
	{
	    acf_add_options_page();
	}

	register_nav_menus( array( 
        'header' => 'Header menu', 
        'footer' => 'Footer menu' 
  	) );

  	add_filter ( 'nav_menu_css_class', 'so_37823371_menu_item_class', 10, 4 );

	function so_37823371_menu_item_class ( $classes, $item, $args, $depth ){
	  $classes[] = 'nav-item';
	  return $classes;
	}

	/*add_filter( 'nav_menu_link_attributes', function($atts) {
        $atts['class'] = "nav-link";
        return $atts;
	}, 	100, 1 );*/

	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

	function special_nav_class ($classes, $item) {
	  if (in_array('current-menu-item', $classes) ){
	    $classes[] = 'active ';
	  }
	  return $classes;
	}

	/*===============sign up user===========*/

	add_action('wp_ajax_folder_contents', 'add_lap_data');
	add_action('wp_ajax_nopriv_folder_contents', 'add_lap_data');

	function add_lap_data() 
	{
		global $wpdb;
		$getData = explode('&', $_POST['variable']);
		foreach($getData as $key=>$value)
		{
		    $value1 = explode('=', $value);
		    $data[$value1[0]] = $value1[1];
		}
		$value =$data['password'];
		$password = password_hash($value, PASSWORD_BCRYPT, [10]);
		$email = str_replace('%40','@',$data['email']);
		$checkUserDetail = check_user_exist($email);
		if(count($checkUserDetail) > 0)
	    {
	    	$result['success'] = false;
	    	$result['msg'] = 'Email already exist';
	    }
	    else
	    {

	    	$mydb = new wpdb('develope_lap', 'develope_lap', 'develope_lap','localhost');
				
			$mydb->insert( 'users', array(
			    'name' => $data['first_name'],
			    'last_name' =>$data['last_name'],
			    'email' =>$email,
			    'phone' =>$data['phone'],
			    'type' =>$data['usertype'],
			    'password' =>$password,
			    'password_hint' =>$data['password'],
		  	));

		  	$wpEmail = explode("@",$email);

	    	$user_login = wp_slash($wpEmail[0]);
		    $user_email = wp_slash($email);
		    $user_pass  = $data['password'];
		 
		    $userdata = compact( 'user_login', 'user_email', 'user_pass' );
		    $user_id = wp_insert_user( $userdata );
		    wp_set_current_user( $user_id );
		    wp_set_auth_cookie( $user_id );
		    $result['success'] = true;
	    	$result['msg'] = 'Registered successfully.';
	    	$result['url'] = 'https://edu-therapeutics.com/';
		}
		echo json_encode($result);exit;

	}
?>