<?php 
 /* Template Name: service */ 

	get_header();

?>

<!-- CCIE Section -->	
<div class="section ccie_RR_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="ccie_RR_wrap">
				
				<div class="row justify-content-center align-items-center">
					<div class="col-md-7 col-sm-12">
						<div class="content_box wow slideInLeft">
							<h3><?php the_field('title');?></h3>
							<?php the_field('subtitle');?>							
						</div>
					</div>
					<div class="col-md-5 col-sm-12 text-center">
						<div class="img_box wow slideInRight">

							<?php $image = get_field('image'); ?>

							<img src="<?php echo $image['url'];?>" class="img-fluid" alt="ccie-right-image">
						</div>
					</div>
				</div>
				
				<div class="ccie_RR_tabs">
					<ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#ccie01" role="tab" aria-controls="pills-home" aria-selected="true">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/1.png';?>" alt="icon">	
									<p>EVE-NG</p>	
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#ccie02" role="tab" aria-controls="pills-profile" aria-selected="false">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/7.png';?>" alt="icon">	
									<p>GNS3</p>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#ccie03" role="tab" aria-controls="pills-contact" aria-selected="false">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/3.png';?>" alt="icon">
									<p>VIRL</p>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#ccie04" role="tab" aria-controls="pills-contact" aria-selected="false">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/4.png';?>" alt="icon">
									<p>Bare-Matal</p>
								</div>
							</a>
						</li>										
					</ul>
					<div class="tab-content" id="pills-tabContent">						
						<div class="tab-pane fade show active" id="ccie01" role="tabpanel" aria-labelledby="pills-home-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>EVE-NG</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>	
						</div>
						<div class="tab-pane fade" id="ccie02" role="tabpanel" aria-labelledby="pills-profile-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>GNS3</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="tab-pane fade" id="ccie03" role="tabpanel" aria-labelledby="pills-contact-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>VIRL</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="tab-pane fade" id="ccie04" role="tabpanel" aria-labelledby="pills-contact-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>Bare-Matal</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>
						</div>											
					</div>
				</div>																
				
			</div>
		</div>
	</div>
</div>
<!-- CCIE Section End -->

<!-- Package Section -->	
<div class="section package_sec">
	<div class="container-fluid">
		<div class="container_space">			
			<div class="package_wrap wow zoomIn">				
				<div class="row">
					<div class="col-xl-1 d-none d-xl-block"></div>
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">Daily</p>
							<h3 class="price">$10.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">More Details</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$50.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">More Details</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$100.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">Learn More</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$265.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">Learn More</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$450.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">Learn More</a>
						</div>
					</div>	
					<div class="col-xl-1 d-none d-xl-block"></div>	
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Package Section End -->

<!-- Feature Section -->	
<div class="section feature_sec">
	<div class="container-fluid">
		<div class="container_space">			
			<div class="feature_wrap">				
				<div class="row">
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3><?php the_field('included_title');?></h3>
							<?php the_field('included_sub_title');?>
							<a href="<?php the_field('more_detail_link');?>"><?php the_field('more_detail_text');?></a>
							<ul>
								<?php 
									if( have_rows('included_detail') ): 
										while( have_rows('included_detail') ): the_row(); 

										$title = get_sub_field('detail');
								?>
									<li><i class="fas fa-check"></i><?php echo $title;?></li>
								<?php endwhile; endif; ?>							
							</ul>							
						</div>
					</div>	
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3><?php the_field('software_detail_title');?></h3>
							<?php the_field('software_detail_sub_title');?>
							<a href="<?php the_field('software_more_detail_link');?>"><?php the_field('software_more_detail_text');?></a>
							<ul>
								<?php 
									if( have_rows('hardware_&_software_detail') ): 
										while( have_rows('hardware_&_software_detail') ): the_row(); 

										$htitle = get_sub_field('h&s_detail');
								?>
									<li><i class="fas fa-check"></i><?php echo $htitle;?></li>
								<?php endwhile; endif; ?>							
							</ul>							
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3><?php the_field('topology_title');?></h3>
							<?php the_field('topology_subtitle');?>
							<a href="<?php the_field('topology_more_detail_link');?>"><?php the_field('topology_more_detail_text');?></a>

							<?php $timage = get_field('topology_image'); ?>

							<img src="<?php echo $timage['url'];?>" class="img-fluid" alt="image">						
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3><?php the_field('how_to_connect_title');?></h3>
							<?php the_field('how_to_connect_sub_title');?>
							<a href="<?php the_field('how_to_connect_more_detail_link');?>"><?php the_field('how_to_connect_more_detail_text');?></a>

							<?php $cimage = get_field('how_to_connect_image'); ?>

							<img src="<?php echo $cimage['url'];?>" class="img-fluid" alt="image">						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Feature Section End -->

<!-- FAQS Section -->	
<div class="section faqs_sec">
	<div class="container-fluid">
		<div class="container_space">		
			<div class="section_head text-center">
				<h3 class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5"><?php the_field('faq_main_title');?></h3>
			</div>
			<div class="faqs_wrap">				
				<div class="row">				
					<div class="col-lg-8 col-md-9 col-sm-12">	
						<?php 
							if( have_rows('faq_detail') ): 
								while( have_rows('faq_detail') ): the_row(); 

								$ftitle = get_sub_field('faq_title');
								$fdesc = get_sub_field('faq_description');
						?>
							<div class="faqs_box">
							<h4><?php echo $ftitle;?></h4>
							<?php echo $fdesc;?>
						</div>
						<?php endwhile; endif; ?>					
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- FAQS Section End -->

<?php get_footer(); ?>