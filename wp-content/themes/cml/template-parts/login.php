<?php 
 /* Template Name: login */ 

	get_header();

?>
<!-- Login Section -->	
<div class="section login_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="login_wrap">
				<div class="row justify-content-center align-items-center">
					<div class="col-md-6 col-sm-12 text-center">
						<div class="img_box">
							<img src="<?php echo get_template_directory_uri().'/img/others/login.png';?>" class="img-fluid" alt="banner-right-image">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="content_box">
							<h3 class="text-center mb-4">Cloudmylab Sign In</h3>
							<p class="text-center mb-5">New To Cloudmylab? <a href="<?php echo home_url('sign-up');?>">Create an Account</a></p>		
							<form class="auth_form">
								<div class="form-group">
									<div class="d-flex">
										<label>Username</label>	
										<a href="<?php echo home_url('forgot-password');?>" class="ml-auto i_forgot">I Forgot</a>
									</div>
									<input type="text" class="form-control" placeholder="Enter Username">
								</div>
								<div class="form-group">
									<div class="d-flex">
										<label>Password</label>	
										<a href="<?php echo home_url('forgot-password');?>" class="ml-auto i_forgot">I Forgot</a>
									</div>
									<input type="password" class="form-control" placeholder="Enter Password">
								</div>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="login-check">
									<label class="form-check-label" for="login-check">Keep me Signed in</label>
								</div>
								<div class="text-left mt-5">
									<button type="button" class="btn btn-block auth_btn">Sign In</button>
								</div>
							</form>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Login Section End -->	
<?php get_footer();?>