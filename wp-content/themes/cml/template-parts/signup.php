<?php 
 /* Template Name: sign_up */ 

	get_header();

?>

<!-- Login Section -->	
<div class="section login_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="login_wrap">
				<div class="row justify-content-center align-items-center">
					<div class="col-md-6 col-sm-12 text-center">
						<div class="img_box">
							<img src="<?php echo get_template_directory_uri().'/img/others/signup.png';?>" class="img-fluid" alt="banner-right-image">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="content_box">
							<h3 class="text-center mb-4">Create an Account</h3>
							<p class="text-center mb-5">Already have an account? <a href="<?php echo home_url('login');?>">Sign In</a></p>		
							<form class="auth_form" autocomplete="off" id="formdata">
								<div class="form-group">
									<label>Email</label>
									<input type="email" id="email" class="form-control input_1" placeholder="Enter Email" required>
								</div>
								<div class="form-group">
									<label>Username</label>
									<input type="text" id="username" class="form-control input_1" placeholder="Enter Username" required>
								</div>
								<div class="form-group">
									<div class="d-flex">
										<label>Password</label>	
										<a href="javascript:void();" id="password" class="ml-auto i_forgot input_1">Show</a>
									</div>
									<input type="password" class="form-control" placeholder="Enter Password" required>
								</div>								
								<div class="text-left mt-5">
									<button type="button" id="create-account" class="btn btn-block auth_btn">Creating Account</button>
								</div>
								<div class="text-left mt-3">
									<p class="text-center mb-5">By creating an account, you agree to cloudmylab 
									<a href="javascript:void">Terms & Conditions</a> and 
									<a href="javascript:void">Privacy Policy</a></p>
								</div>
							</form>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();?>

