<?php 
 /* Template Name: home */ 

	get_header();

?>

<!-- Banner Section -->	

<div class="section banner_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="banner_wrap">
				<div class="row justify-content-center align-items-center">
					<div class="col-md-7 col-sm-12">
						<div class="content_box wow slideInLeft">
							<h3><?php the_field('hosted_services_title');?></h3>
							<?php the_field('hosted_services_description');?>
						</div>
					</div>
					<div class="col-md-5 col-sm-12 text-center">
						<div class="img_box wow slideInRight">
							<?php 
								$image = get_field('hosted_services_image');
							?>
							<img src="<?php echo $image['url'];?>" class="img-fluid" alt="banner-right-image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banner Section End -->	

<!-- Services Section -->	
<div class="section services_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="section_head text-center">

				<?php $service_image = get_field('our_services_image'); ?>

				<img src="<?php echo $service_image['url'];?>" class="wow bounceIn">

				<h3 class="wow slideInRight" data-wow-duration="2s" data-wow-delay="0.5s"><?php echo the_field('our_services_title');?></h3>

				<p class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5s"><?php echo the_field('our_services_subtitles');?></p>

			</div>
			<div class="services_wrap wow zoomIn" data-wow-delay="0.3s">				
				<div class="row">
					<?php 
						if( have_rows('our_services_detail') ): 
							while( have_rows('our_services_detail') ): the_row(); 

							$image = get_sub_field('image');
							$title = get_sub_field('title');
							$description = get_sub_field('description');
							$read_more = get_sub_field('read_more_title');
							$read_more_link = get_sub_field('read_more_link');

					?>
							<div class="col-md-4 col-sm-12">
								<div class="content_box">
									<img src="<?php echo $image['url'];?>">
									<h3><?php echo $title;?></h3>
									<?php echo $description?>
									<a href="<?php echo $read_more_link;?>"><?php echo $read_more;?></a>
								</div>
							</div>	
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Services Section End -->

<!-- Why G0 With Section -->	
<div class="section gowith_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="section_head text-center">
				<h3 class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5"><?php echo the_field('cloudmylab_title');?></h3>
			</div>
			<div class="gowith_wrap wow zoomIn" data-wow-delay="0.3s">				
				<div class="row">
					<?php 
						if( have_rows('cloudmylab_detail') ): 
							while( have_rows('cloudmylab_detail') ): the_row(); 

							$clouddetailtitle = get_sub_field('cloudmylab_detail_title');
							$clouddetaildesc = get_sub_field('cloudmylab_detail_description');
					?>
							<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="content_box">
									<h3><?php echo $clouddetailtitle;?></h3>
									<?php echo $clouddetaildesc;?>
								</div>
							</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Why G0 With Section End -->	

<!-- What People Say Section -->	
<div class="section people_say_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="section_head text-center">
				<h3 class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5">What People Says About Us?</h3>
			</div>
			<div class="people_say_wrap">
				<div class="people_say_slider owl-carousel owl-theme">
					<div class="item">
						<div class="content_box">
							<img src="<?php echo get_template_directory_uri().'/img/client/1.png';?>" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>
					<div class="item">
						<div class="content_box">
							<img src="<?php echo get_template_directory_uri().'/img/client/1.png';?>" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>
					<div class="item">
						<div class="content_box">
							<img src="<?php echo get_template_directory_uri().'/img/client/1.png';?>" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>
					<div class="item">
						<div class="content_box">
							<img src="<?php echo get_template_directory_uri().'/img/client/1.png';?>" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>	
				</div>				
			</div>
		</div>
	</div>
</div>
<!-- What People Say Section End -->

<?php get_footer(); ?>