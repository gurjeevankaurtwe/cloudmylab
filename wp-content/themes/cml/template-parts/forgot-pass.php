<?php 
 /* Template Name: forgot_password */ 

	get_header();

?>


<!-- Login Section -->	
<div class="section login_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="login_wrap">
				<div class="row justify-content-center align-items-center">
					<div class="col-md-6 col-sm-12 text-center">
						<div class="img_box">
							<img src="<?php echo get_template_directory_uri().'/img/others/login.png';?>" class="img-fluid" alt="banner-right-image">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="content_box">
							<h3 class="text-center mb-4">Forgot Password</h3>
							<p class="text-center mb-5">Remember Password? <a href="<?php echo home_url('login');?>">Sign In</a></p>		
							<form class="auth_form">
								<div class="form-group">
									<label>Registered Email</label>									
									<input type="email" class="form-control" placeholder="Enter Registered Email">
								</div>																
								<div class="text-left mt-5">
									<button type="button" class="btn btn-block auth_btn">Reset Password</button>
								</div>
							</form>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Login Section End -->	

<?php get_footer();?>