<!DOCTYPE html>
<html>

<head>
	<title>Cloud My Lab || Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/style.css';?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/style-fonts.css';?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri().'/img/logo/logo.png';?>">
</head>

<body>