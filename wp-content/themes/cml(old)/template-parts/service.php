<?php 
 /* Template Name: service */ 

	get_header();

?>

<!-- CCIE Section -->	
<div class="section ccie_RR_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="ccie_RR_wrap">
				
				<div class="row justify-content-center align-items-center">
					<div class="col-md-7 col-sm-12">
						<div class="content_box">
							<h3>Hosted Services</h3>
							<p>Hosted Services solved the biggest issue in the industry. The demands of the modern world is overwhelming and the biggest challenge that every network professional faces is time. The world is changing and so must you.</p>							
						</div>
					</div>
					<div class="col-md-5 col-sm-12 text-center">
						<div class="img_box">
							<img src="<?php echo get_template_directory_uri().'/img/others/service.png';?>" class="img-fluid" alt="ccie-right-image">
						</div>
					</div>
				</div>
				
				<div class="ccie_RR_tabs">
					<ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#ccie01" role="tab" aria-controls="pills-home" aria-selected="true">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/1.png';?>" alt="icon">	
									<p>EVE-NG</p>	
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#ccie02" role="tab" aria-controls="pills-profile" aria-selected="false">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/7.png';?>" alt="icon">	
									<p>GNS3</p>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#ccie03" role="tab" aria-controls="pills-contact" aria-selected="false">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/3.png';?>" alt="icon">
									<p>VIRL</p>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#ccie04" role="tab" aria-controls="pills-contact" aria-selected="false">
								<div class="ccie_RR_list">
									<img src="<?php echo get_template_directory_uri().'/img/service/4.png';?>" alt="icon">
									<p>Bare-Matal</p>
								</div>
							</a>
						</li>										
					</ul>
					<div class="tab-content" id="pills-tabContent">						
						<div class="tab-pane fade show active" id="ccie01" role="tabpanel" aria-labelledby="pills-home-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>EVE-NG</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>	
						</div>
						<div class="tab-pane fade" id="ccie02" role="tabpanel" aria-labelledby="pills-profile-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>GNS3</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="tab-pane fade" id="ccie03" role="tabpanel" aria-labelledby="pills-contact-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>VIRL</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="tab-pane fade" id="ccie04" role="tabpanel" aria-labelledby="pills-contact-tab">
							<div class="tab_content">
								<div class="row">
									<div class="col-lg-6 col-md-12">
										<h3>Bare-Matal</h3>									
									</div>
									<div class="col-lg-6 col-md-12 text-lg-right text-md-left">
										<div class="tabs_link">
											<a href="javascript:void();">calculation</a>								
											<a href="javascript:void();">Schedule</a>								
											<a href="javascript:void();">Free Trial</a>								
										</div>
									</div>
								</div>	
							</div>
						</div>											
					</div>
				</div>																
				
			</div>
		</div>
	</div>
</div>
<!-- CCIE Section End -->

<!-- Package Section -->	
<div class="section package_sec">
	<div class="container-fluid">
		<div class="container_space">			
			<div class="package_wrap">				
				<div class="row">
					<div class="col-xl-1 d-none d-xl-block"></div>
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">Daily</p>
							<h3 class="price">$10.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">More Details</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$50.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">More Details</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$100.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">Learn More</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$265.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">Learn More</a>
						</div>
					</div>	
					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-12">
						<div class="content_box">
							<p class="token">10 TOKENS</p>
							<h3 class="price">$450.00</h3>
							<p class="token_price">10 Tokens / $0.42/hr</p>
							<a href="javascript:void();" class="add_cart">Add To Cart</a>
							<ul>
								<li>First Session Free</li>
								<li>1 session</li>
								<li>24 hours</li>
								<li>24 rack hours</li>								
							</ul>
							<a href="javascript:void();" class="more_details">Learn More</a>
						</div>
					</div>	
					<div class="col-xl-1 d-none d-xl-block"></div>	
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Package Section End -->

<!-- Feature Section -->	
<div class="section feature_sec">
	<div class="container-fluid">
		<div class="container_space">			
			<div class="feature_wrap">				
				<div class="row">
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3>What's Included</h3>
							<p >R&Sv5 Rack Access Guide (PDF file) <br>CCIE RS V5 Access Guide</p>
							<a href="javascript:void();">More Details</a>
							<ul>
								<li><i class="fas fa-check"></i> 20 Branch routers 15.4.1T</li>
								<li><i class="fas fa-check"></i> Universal release 3 Backbone routers 15.4.1T</li>
								<li><i class="fas fa-check"></i> Universal release 5 ISP routers 15.4.1T</li>
								<li><i class="fas fa-check"></i> Universal release 6 switches 15.1</li>								
								<li><i class="fas fa-check"></i> Universal (IP Services)</li>								
							</ul>							
						</div>
					</div>	
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3>Hardware & Software Details</h3>
							<p>Universal Software release <br>15.4.1T</p>
							<a href="javascript:void();">More Details</a>
							<ul>
								<li><i class="fas fa-check"></i> 20 Branch routers 15.4.1T</li>
								<li><i class="fas fa-check"></i> Universal release 3 Backbone routers 15.4.1T</li>
								<li><i class="fas fa-check"></i> Universal release 5 ISP routers 15.4.1T</li>
								<li><i class="fas fa-check"></i> Universal release 6 switches 15.1</li>								
								<li><i class="fas fa-check"></i> Universal (IP Services)</li>								
							</ul>							
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3>Topology</h3>
							<p>As per the workbook vendor</p>
							<a href="javascript:void();">More Details</a>
							<img src="<?php echo get_template_directory_uri().'/img/others/ccie.png';?>" class="img-fluid" alt="image">						
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-12">
						<div class="content_box">
							<h3>How To Connect</h3>
							<p >R&Sv5 Rack Access Guide (PDF file) <br>CCIE RS V5 Access Guide</p>
							<a href="javascript:void();">More Details</a>
							<img src="<?php echo get_template_directory_uri().'/img/others/01.png';?>" class="img-fluid" alt="image">						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Feature Section End -->

<!-- FAQS Section -->	
<div class="section faqs_sec">
	<div class="container-fluid">
		<div class="container_space">		
			<div class="section_head text-center">
				<h3>Frequently asked questions about domains.</h3>
			</div>
			<div class="faqs_wrap">				
				<div class="row">				
					<div class="col-lg-8 col-md-9 col-sm-12">				
						<div class="faqs_box">
							<h4>Features support- Cisco Nodes on EVE-NG ?</h4>
							<p>Means an existing Network/Lab or Hosted Service is down or there is a critical impact to End User’s business operation. End User and Cloudmylab both will commit full-time resources to resolve the situation.</p>
						</div>
						<div class="faqs_box">
							<h4>How Can I Cancel my Hosted Service Subscription ?</h4>
							<p>In the event that you want to cancel your subscription before even starting your first access, we will cancel your subscription and refund 90% of your payment. 10% is dedicated to the time and resource spent on setting up our lab.</p>
						</div>
						<div class="faqs_box">
							<h4>EVE-NG Community Lab Access Guide</h4>
							<p>Means an existing Network/Lab or Hosted Service is down or there is a critical impact to End User’s business operation. End User and Cloudmylab both will commit full-time resources to resolve the situation.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- FAQS Section End -->

<?php get_footer(); ?>