<?php 
 /* Template Name: home */ 

	get_header();

?>

<!-- Banner Section -->	
<div class="section banner_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="banner_wrap">
				<div class="row justify-content-center align-items-center">
					<div class="col-md-7 col-sm-12">
						<div class="content_box wow slideInLeft">
							<h3>Premier Cloud, and Hosted Services</h3>
							<p>CloudMyLab was born in 2016 with an idea to innovate and take the Proof of Concept and Certification labs to the next-level.</p>
							<p>As a team of CCIE's and Network Engineers / Consultants, we realized that there should be a platform, where anybody could go and run any proof of concept of any Virtual / Physical device for that matter. After a lot of research, we found that there were no options which were cost-effective or got us what we need. We created our awesome cloud platform with robust infrastructure build on Cisco UCS an Nexus with best practices and state of the art design.</p>
							<p>CloudMyLab is an Infrastructure-As-A-Service (IAAS) provider and we can practically simulate any Lab or POC evironment. We are in the heart of Sillicon Valley and have an awesome data center in Fremont, CA.</p>
						</div>
					</div>
					<div class="col-md-5 col-sm-12 text-center">
						<div class="img_box wow slideInRight">
							<img src="img/banner/1.png" class="img-fluid" alt="banner-right-image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banner Section End -->	

<!-- Services Section -->	
<div class="section services_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="section_head text-center">
				<img src="img/icons/rocket.png" class="wow bounceIn">
				<h3 class="wow slideInRight" data-wow-duration="2s" data-wow-delay="0.5s">Our services are aimed at keeping your <br>networks fully operational,</h3>
				<p class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5s">CloudMyLab is on a mission to bring technical competence, virtual infrastructure and <br>leading hosted services to every business seeking a powerful competitive edge.</p>
			</div>
			<div class="services_wrap wow zoomIn" data-wow-delay="0.3s">				
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="content_box">
							<img src="img/service/1.png">
							<h3>Services</h3>
							<p>As a valued and reputable CCIE rack rental provider, we help you choose the solution to best.</p>
							<a href="javascript:void();">Read More</a>
						</div>
					</div>	
					<div class="col-md-4 col-sm-12">
						<div class="content_box">
							<img src="img/service/2.png">
							<h3>CCIE Rack Rentals</h3>
							<p>As a valued and reputable CCIE rack rental provider, we help you choose the solution to best.</p>
							<a href="javascript:void();">Read More</a>
						</div>
					</div>	
					<div class="col-md-4 col-sm-12">
						<div class="content_box">
							<img src="img/service/3.png">
							<h3>Learning Labs</h3>
							<p>As a valued and reputable CCIE rack rental provider, we help you choose the solution to best.</p>
							<a href="javascript:void();">Read More</a>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Services Section End -->

<!-- Why G0 With Section -->	
<div class="section gowith_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="section_head text-center">
				<h3 class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5">Why go with Cloudmylab?</h3>
			</div>
			<div class="gowith_wrap wow zoomIn" data-wow-delay="0.3s">				
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="content_box">
							<h3>Fast And Reliable</h3>
							<p>Your time is important to us. We have our services hosted in state of the art datacenter with electric and internet backup. We have a 10Gbps Primary and 2 Gbps bonded backup for connectivity. In 2016, we achieved 99.99% SLA.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="content_box">
							<h3>Lifetime Tokens</h3>
							<p>Purchased tokens never expire. You can book your labs anytime you want, allowing you to do a one-time purchase and use it at a later time. This will give you the advantage of our discounts and promos.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="content_box">
							<h3>24/7 Support</h3>
							<p>We do not want to leave you unattended. Our amazing 24/7 technical support is always available to jump into action to fix any problem you may have or answer any queries you have in mind.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="content_box">
							<h3>100% Up-time</h3>
							<p>Our racks are built with advanced Cisco platforms and technology. From UCS server to host various virtual machines to our state of the art Cisco FirePower Firewalls. All physical devices are properly mounted and maintained at our secured data center in Fremont California with proper environmental control to ensure all devices are working in perfect condition.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="content_box">
							<h3>Custom Lab Support</h3>
							<p>Our racks support almost all CCIE workbooks such as Cisco 360, INE, CCIECert and Micronics. We also accommodate custom lab setup to suit your study needs.
							Please reach out to our support alias or open a ticket by clicking on support.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="content_box">
							<h3>Custom Hosting</h3>
							<p>CloudMyLab offers tailor-made suite of services, which you can use for DEMOS, proof of concepts, proof of value or you need couple of high powered servers for your devops team. Give us your wish list and we promise we will make it happen.</p>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Why G0 With Section End -->	

<!-- What People Say Section -->	
<div class="section people_say_sec">
	<div class="container-fluid">
		<div class="container_space">
			<div class="section_head text-center">
				<h3 class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.5">What People Says About Us?</h3>
			</div>
			<div class="people_say_wrap">
				<div class="people_say_slider owl-carousel owl-theme">
					<div class="item">
						<div class="content_box">
							<img src="img/client/1.png" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>
					<div class="item">
						<div class="content_box">
							<img src="img/client/1.png" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>
					<div class="item">
						<div class="content_box">
							<img src="img/client/1.png" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>
					<div class="item">
						<div class="content_box">
							<img src="img/client/1.png" alt="people-image">
							<div class="rating">
								<input type="radio" id="star1" name="rating"><label for="star1"></label>
								<input type="radio" id="star2" name="rating"><label for="star2"></label>
								<input type="radio" id="star3" name="rating"><label for="star3"></label>
								<input type="radio" id="star4" name="rating"><label for="star4"></label>
								<input type="radio" id="star5" name="rating"><label for="star5"></label>							  
							</div>	
							<p>In General Big data hadoop, (a) you can learn on your personal PC, but for that the minimum configuration of 12 GB Ram with good processing speed, still when you execute jobs it will take more time for processing</p>
							<h3>Fast And Reliable</h3>							
							<h4>Orlando, Florida</h4>							
						</div>
					</div>	
				</div>				
			</div>
		</div>
	</div>
</div>
<!-- What People Say Section End -->

<?php get_footer(); ?>