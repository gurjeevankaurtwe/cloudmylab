<?php
	if( function_exists('acf_add_options_page') ) 
	{
	    acf_add_options_page();
	}

	register_nav_menus( array( 
        'header' => 'Header menu', 
        'footer' => 'Footer menu' 
  	) );

  	add_filter ( 'nav_menu_css_class', 'so_37823371_menu_item_class', 10, 4 );

	function so_37823371_menu_item_class ( $classes, $item, $args, $depth ){
	  $classes[] = 'nav-item';
	  return $classes;
	}

	/*add_filter( 'nav_menu_link_attributes', function($atts) {
        $atts['class'] = "nav-link";
        return $atts;
	}, 	100, 1 );*/

	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

	function special_nav_class ($classes, $item) {
	  if (in_array('current-menu-item', $classes) ){
	    $classes[] = 'active ';
	  }
	  return $classes;
	}
?>