<!-- What People Say Section -->	
<div class="need_help_sec">
	<div class="container-fluid">
		<div class="need_help_wrap text-center">
			<h3>Need help? Call our support team 24/7 at 040 01526547</h3>
		</div>
	</div>
</div>
<!-- What People Say Section End -->

<!-- Footer Section -->
<footer class="footer">
	<div class="container-fluid">
		<div class="container_space foot_space">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3>Contact Info</h3>
						<ul>
							<li>
								<p>Corporate Commons, 6200 Stoneridge<br>Mall Road, 3rd Floor,<br>Pleasanton,CA,94588</p>
							</li>
							<li>
								<p>info@cloudmylab.com</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3>Quick Link</h3>
						<ul>
							<li><a href="javascript:void();">Home</a></li>
							<li><a href="javascript:void();">CCIE Rental Racks</a></li>
							<li><a href="javascript:void();">Hosted Services</a></li>
							<li><a href="javascript:void();">Cisco Certification</a></li>
							<li><a href="javascript:void();">Blogs</a></li>							
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3>Important Link</h3>
						<ul>
							<li><a href="javascript:void();">FAQS</a></li>
							<li><a href="javascript:void();">About Us</a></li>
							<li><a href="javascript:void();">Contact Us</a></li>
							<li><a href="javascript:void();">Terms & Conditions</a></li>
							<li><a href="javascript:void();">Privacy Policy</a></li>							
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="foot_box">
						<h3>Subscribe Us</h3>
						<div class="subs_box">
							<form>
								<input type="email" placeholder="Email">
								<span><i class="fas fa-paper-plane"></i></span>
							</form>
						</div>
						<h3>Follow Us</h3>
						<div class="social_box">
							<ul>
								<li><a href="javascript:void();"><i class="fab fa-facebook-f"></i></a></li>															
								<li><a href="javascript:void();"><i class="fab fa-twitter"></i></a></li>															
								<li><a href="javascript:void();"><i class="fab fa-instagram"></i></a></li>															
								<li><a href="javascript:void();"><i class="fab fa-pinterest-p"></i></a></li>															
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Footer Section End -->
	
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="js/owl.carousel.min.js"></script>

<script>
$(document).ready(function(){
	$('.people_say_slider').owlCarousel({
		items:3,
		loop:true,
		margin:30,
		nav:false,
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
			},
			600:{
				items:2,
			},
			1000:{
				items:3,
			}
		}
	});
});
</script>

<script>
	new WOW().init();
</script>
	 
</body>
</html>