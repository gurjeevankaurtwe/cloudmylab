<!DOCTYPE html>
<html>

<head>
	<title>Cloud My Lab || Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/style-fonts.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.default.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link rel="shortcut icon" type="image/png" href="img/logo/logo.png">
	
</head>

<body>

<!-- Header Section -->	
<header class="header">
	<div class="container-fluid">
		<div class="container_space">
			<nav class="navbar navbar-expand-lg">
				<a class="navbar-brand" href="index.php"><img src="img/logo/logo.png"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fas fa-bars"></i>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
					  <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
					  <li class="nav-item"><a class="nav-link" href="service.php">Services</a></li>
					  <li class="nav-item"><a class="nav-link" href="ccie.php">CCIE Rack Rentals</a></li>
					  <li class="nav-item"><a class="nav-link" href="lab-as-service.php">Learning Labs</a></li>
					  <li class="nav-item"><a class="nav-link" href="poc-as-service.php">Price</a></li>
					  <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>			  
					  <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" href="javascript:void();"><i class="fas fa-search"></i> Search</a></li>			  
					  <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" href="javascript:void();"><i class="fas fa-shopping-bag"></i> Bags</a></li>			  
					</ul>			
				</div>
				<ul class="navbar-nav extra_nav d-none d-lg-block d-xl-block">
				  <li><a href="javascript:void();"><i class="fas fa-search"></i></a></li>
				  <li><a href="javascript:void();"><i class="fas fa-shopping-bag"></i></a></li>
				</ul>		
			</nav>
		</div>
	</div>
</header>	
<!-- Header Section End -->