<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cloud_my_lab');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F9Cl]h!hp5cEHYiT7r>+Fn7n&6J!);170IsDG{:!FQ`kW?qEd4=%KTX}iek;6QP[');
define('SECURE_AUTH_KEY',  '`/! &7GS{nqxB%z.`o)*N9NFYBhwk{SEJj|1Ado.,|zksC#i5qAeOn24CpolQd^U');
define('LOGGED_IN_KEY',    '807,5TjZI]GxY9~DOf 541tHNEDUWN0b&dVrDvTkV|,34U=+aMxC{7DlX` )mm$F');
define('NONCE_KEY',        '=-AE@&RUW0)v=z[qmd/rQnn$;|{dwh.HAY[s/Fy?G>LcI~r%ZeXC^R,5,:!n-I+9');
define('AUTH_SALT',        'IN09(_EZi+&3eAQgBYle!SBf6oDSXj8w$I(nJOb$+mo_kpyTEg5i7(a3,dX<.t$T');
define('SECURE_AUTH_SALT', 'dVH^q+Rs+ZGN}3SN73EXm[gpUpbs/~ n38+Ne2C@+9. Me:HQ1XLN:0_5r@-BPje');
define('LOGGED_IN_SALT',   '!^xdjz>m_iF-mWh$voB)f)!g@<Y*:m&dpB|kDq#sT(@g(a[4DTBL4pIJP-%?XHn6');
define('NONCE_SALT',       '.QID&B:</Yco;6T)a%&[YQt4b3)(^l +$E9A/;m`eQXgA8 S& gAxKd_%2]E/xBm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
@ini_set( 'display_errors', 0 );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
